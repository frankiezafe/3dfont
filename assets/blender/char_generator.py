import bpy, os

TMPL = bpy.data.objects['char']
EMPTY_CHAR = bpy.data.objects['empty_char']

CHAR_START = 0
CHAR_END = 300

EXPORT_PATH = '//../../godot/fonts/Facade-Sud/imports/'
EXPORT_INDIVIDUAL = False
EXPORT_SET = True

coll_target = bpy.context.scene.collection.children.get("exports")

valid_chars = 0

# cleaning exports collection
zombies = []
for o in bpy.data.objects:
    if o == TMPL or o == EMPTY_CHAR:
        continue
    for coll in o.users_collection:
        if coll.name == 'exports':
            zombies.append( o )
            break
for z in zombies:
    bpy.context.view_layer.objects.active = z
    z.select_set(True)
    bpy.ops.object.delete(use_global=False)

char_set = []

for i in range( CHAR_START, CHAR_END+1 ):
    
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = TMPL
    TMPL.select_set(True)
    
    bpy.ops.object.duplicate()
    char_obj = bpy.context.object
    
    char_obj.data.body = chr(i)
    cname = str(i)
    while len( cname ) < 3:
        cname = '0' + cname
    char_obj.name = 'c' + cname
    bpy.ops.object.convert(target='MESH')
    char_obj.data.name = 'c' + cname
    
    if len( char_obj.data.vertices ) == 0:
        # no character here!
        bpy.ops.object.delete(use_global=False)
        print( '&#'+str(i), ' is empty' )
        continue
    
    # is it an empty char?
    if len( char_obj.data.vertices ) == len( EMPTY_CHAR.data.vertices ):
        identical = True
        for i in range( 0, len( char_obj.data.vertices ) ):
            if char_obj.data.vertices[i].co != EMPTY_CHAR.data.vertices[i].co:
                identical = False
                break
        if identical:
            bpy.ops.object.delete(use_global=False)
            print( '&#'+str(i), ' is considered as empty' )
            continue
    
    # uv generation
    bpy.ops.mesh.uv_texture_add()
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.uv.unwrap(method='ANGLE_BASED', margin=0.001)
    bpy.ops.object.editmode_toggle()
    
    print( '&#'+str(i), ' successfully created' )
    valid_chars += 1
    
    for coll in char_obj.users_collection:
        # Unlink the object
        coll.objects.unlink(char_obj)
    coll_target.objects.link(char_obj)
    
    if EXPORT_INDIVIDUAL:
        cpath = os.path.join( bpy.path.abspath( EXPORT_PATH ), 'c' + cname )
        bpy.ops.export_scene.gltf(filepath=cpath, check_existing=False, export_format='GLB', export_materials='NONE', export_colors=False, export_selected=True)
        
    char_set.append( char_obj )

if EXPORT_SET and len( char_set ) > 0:
    bpy.ops.object.select_all(action='DESELECT')
    for c in char_set:
        c.select_set(True)
    fpath = os.path.join( bpy.path.abspath( EXPORT_PATH ), 'font' )
    bpy.ops.export_scene.gltf(filepath=fpath, check_existing=False, export_format='GLB', export_materials='NONE', export_colors=False, export_selected=True)

print( valid_chars, ' characters generated' )

#bpy.ops.outliner.delete(hierarchy=True)