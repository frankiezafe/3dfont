tool

extends Spatial

export(String) var font_folder = ''
export(Dictionary) var font_info:Dictionary = {
	'characters': []
}
export(bool) var generate:bool = false setget set_generate

func set_generate(b:bool) -> void:
	generate = false
	if b:
		while $import.get_child_count() > 0:
			$import.remove_child( $import.get_child(0) )
		var source_ps:PackedScene = load( font_folder + '/imports/font.glb' )
		var source:Spatial = source_ps.instance()
		$import.add_child( source )
		source.owner = $import.owner
		font_info = {}
		font_info.characters = []
		for c in source.get_children():
			var char_info:Dictionary = {}
			var cnum:int = c.name.substr(1).to_int()
			char_info.ord = cnum
			char_info.char = char(cnum)
			char_info.path = font_folder + 'meshes/'+str(cnum)+'.mesh'
			char_info.min = Vector3.ZERO
			char_info.max = Vector3.ZERO
			
			var surface:Array = c.mesh.surface_get_arrays(0)[Mesh.ARRAY_VERTEX]
			for i in range(0,surface.size()):
				if i == 0:
					char_info.min = surface[i]
					char_info.max = surface[i]
				else:
					for axis in range(0,3):
						if char_info.min[axis] > surface[i][axis]:
							char_info.min[axis] = surface[i][axis]
						if char_info.max[axis] < surface[i][axis]:
							char_info.max[axis] = surface[i][axis]
			char_info.size = char_info.max - char_info.min
			
			font_info.characters.append( char_info )
			ResourceSaver.save( char_info.path, c.mesh, ResourceSaver.FLAG_REPLACE_SUBRESOURCE_PATHS )
		
		var font_gd:File = File.new()
		font_gd.open( font_folder + 'font.gd', File.WRITE )
		font_gd.store_line( 'extends Node' )
		font_gd.store_line( 'export(Dictionary) var data:Dictionary = {' )
		font_gd.store_line( '\t'+"'characters':[" )
		for c in font_info.characters:
			var esc:String = ''
			if c.char == '\'' or c.char == '\"' or c.char == '\\':
				esc = '\\'
			var l:String = '\t\t'+"{ 'ord':"+ str(c.ord) + ", 'char':'"+ esc + c.char +"', 'path':'"+ c.path + "',"
			l += "'min': ["+ str(c.min[0]) + ","+ str(c.min[1]) + ","+ str(c.min[2]) + "],"
			l += "'max': ["+ str(c.max[0]) + ","+ str(c.max[1]) + ","+ str(c.max[2]) + "],"
			l += "'size': ["+ str(c.size[0]) + ","+ str(c.size[1]) + ","+ str(c.size[2]) + "]"
			l += "},"
			font_gd.store_line( l )
		font_gd.store_line( '\t]' )
		font_gd.store_line( '}' )
		font_gd.close()
		
		$render.font_folder = font_folder
